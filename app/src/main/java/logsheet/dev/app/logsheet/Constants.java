package logsheet.dev.app.logsheet;

import android.annotation.SuppressLint;

/**
 * Created by khairulimam on 14/03/18.
 */

public class Constants {
  public static final int PORT = 8000;
  public static final String HP_NET_ADDRESS = "192.168.43.110";
  public static final String USB_NET_ADDRESS = "192.168.42.162";
  public static final String LSP_NET_ADDRESS = "172.16.16.155";
  public static final String LSP_NET_WIFI_ADDRESS = "172.16.22.37";
  public static final String INTERNET_URL = "https://logsheet.herokuapp.com";

  public static final String OPERATOR_NAME_KEY = "OPERATOR_NAME_KEY";
  public static final String SHARED_PREF_NAME = "SHARED_PREF_NAME";
  public static final String defaultOperatorName = "Default Operator Name";

  public static final String QR_CODE_RESULT_INTENT_KEY = "QR_CODE_RESULT_INTENT_KEY";

  @SuppressLint("DefaultLocale")
  public static final String LOCAL_URL = String.format("http://%s:%d", HP_NET_ADDRESS, PORT);
  public static final String BASE_URL = String.format("%s/api/", INTERNET_URL);
}
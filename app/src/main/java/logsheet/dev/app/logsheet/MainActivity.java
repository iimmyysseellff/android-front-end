package logsheet.dev.app.logsheet;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import logsheet.dev.app.logsheet.adapters.MachineSectionPagerAdapter;
import logsheet.dev.app.logsheet.api.APIConsumer;
import logsheet.dev.app.logsheet.api.CreateGson;
import logsheet.dev.app.logsheet.api.model.MachineStatus;
import logsheet.dev.app.logsheet.controllers.FormModalDialogController;
import logsheet.dev.app.logsheet.fragments.MachineStatusFragment;
import logsheet.dev.app.logsheet.interfaces.AfterDataSent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements PermissionListener, AfterDataSent, DialogInterface.OnClickListener {

  /**
   * The {@link ViewPager} that will host the section contents.
   */
  @BindView(R.id.container)
  protected ViewPager mViewPager;
  @BindView(R.id.toolbar)
  protected Toolbar toolbar;
  @BindView(R.id.tabs)
  protected TabLayout tabLayout;
  @BindView(R.id.fab)
  protected FloatingActionButton fab;

  private static final String LOGTAG = MainActivity.class.getName();

  private SharedPreferences sharedPreferences;
  private SharedPreferences.Editor editor;
  private String operatorName = "";

  private MachineSectionPagerAdapter mMachineSectionsPagerAdapter;
  private Map<String, ArrayList<MachineStatus>> statusesGroupByMachineType = new HashMap<>();
  private List<MachineStatus> machineStatusList = new ArrayList<>();
  private Snackbar snackbarInfo;
  private FormModalDialogController formModalDialogController;

  private AlertDialog.Builder editOperatorNameDialog;
  private EditText etNewOperatorName;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_activity);
    ButterKnife.bind(this);
    sharedPreferences = getSharedPreferences(Constants.SHARED_PREF_NAME, MODE_PRIVATE);
    editor = sharedPreferences.edit();
    operatorName = sharedPreferences.getString(Constants.OPERATOR_NAME_KEY, Constants.defaultOperatorName);

    toolbar.setSubtitle(operatorName);
    setSupportActionBar(toolbar);

    TedPermission.with(this)
        .setPermissionListener(this)
        .setDeniedMessage(R.string.if_no_permission)
        .setPermissions(requestThisPermissions())
        .check();

    snackbarInfo = Snackbar.make(mViewPager, R.string.wait_getting_data_text, Snackbar.LENGTH_INDEFINITE);
    formModalDialogController = new FormModalDialogController(this, mViewPager, this);

    fab.setOnClickListener(view -> {
      Intent i = new Intent(MainActivity.this, ScanMachineQrCodeActivity.class);
      startActivityForResult(i, ScanMachineQrCodeActivity.ACTIVITY_RESULT_CODE);
    });

    snackbarInfo.show();
    mMachineSectionsPagerAdapter = new MachineSectionPagerAdapter(getSupportFragmentManager());
    APIConsumer.consume().machineLogs().enqueue(machineStatusResponse());
    tabLayout.setupWithViewPager(mViewPager);
  }

  private String[] requestThisPermissions() {
    return new String[]{
        Manifest.permission.INTERNET,
        Manifest.permission.VIBRATE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA};
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_machine_grouped_by_log_category, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      showEditOperatornameDialog();
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  private void showEditOperatornameDialog() {
    editOperatorNameDialog = new AlertDialog.Builder(MainActivity.this);
    editOperatorNameDialog.setCancelable(false);
    etNewOperatorName = new EditText(this);
    etNewOperatorName.setHint("Masukkan nama operator baru");
    editOperatorNameDialog.setTitle(R.string.edit_operator_name);
    editOperatorNameDialog.setView(etNewOperatorName);
    editOperatorNameDialog.setPositiveButton("Simpan", this);
    editOperatorNameDialog.setNegativeButton("Kembali", (dialog, which) -> dialog.dismiss());
    editOperatorNameDialog.show();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode != RESULT_OK) {
      Snackbar.make(mViewPager, R.string.scan_unsuccessful, Snackbar.LENGTH_LONG).show();
      return;
    }
    if (requestCode == ScanMachineQrCodeActivity.ACTIVITY_RESULT_CODE) {
      String result = data.getStringExtra(Constants.QR_CODE_RESULT_INTENT_KEY);
      new HandleScanResultOnBackground().execute(result);
    }
  }

  @Override
  public void onClick(DialogInterface dialog, int which) {
    switch (which) {
      case DialogInterface.BUTTON_POSITIVE:
        if (etNewOperatorName != null) {
          String newNamaOperator = etNewOperatorName.getText().toString();
          if ((newNamaOperator.length() <= 0)
              || (newNamaOperator == null)) {
            Snackbar.make(mViewPager, "Nama operator tidak boleh kosong!", 5000).show();
            return;
          }
          editor.putString(Constants.OPERATOR_NAME_KEY, newNamaOperator);
          editor.commit();
          toolbar.setSubtitle(newNamaOperator);
          Snackbar.make(mViewPager, "Nama operator berhasil disimpan", 5000).show();
        }
        break;
    }
  }

  private class HandleScanResultOnBackground extends AsyncTask<String, Void, MachineStatus> {

    private MaterialDialog alertDialog;

    @Override
    protected void onPreExecute() {
      alertDialog = new MaterialDialog.Builder(MainActivity.this)
          .cancelable(false)
          .content(R.string.wait_building_form)
          .progress(true, 0)
          .show();
    }

    @Override
    protected MachineStatus doInBackground(String... strings) {
      int machineId = Integer.parseInt(strings[0]);

      MachineStatus foundMachine = Stream.of(machineStatusList).filter(value -> {
        return value.getId() == machineId;
      }).findFirst().get();

      return foundMachine;
    }

    @Override
    protected void onPostExecute(MachineStatus machineStatus) {
      alertDialog.dismiss();
      if (machineStatus.getReportStatus()) {
        Snackbar.make(mViewPager,
            getResources().getString(R.string.machine_scanned_text, machineStatus.getName(), machineStatus.getPlaceholder()),
            Snackbar.LENGTH_LONG)
            .show();
        return;
      }
      formModalDialogController.setUpFormDialogForMachine(machineStatus);
    }
  }

  private Callback<JsonElement> machineStatusResponse() {
    return new Callback<JsonElement>() {
      @Override
      public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
        JsonObject jsonObjectResponse = response.body().getAsJsonObject();
        statusesGroupByMachineType = CreateGson.instance()
            .fromJson(jsonObjectResponse.toString(), new TypeToken<HashMap<String, ArrayList<MachineStatus>>>() {
            }.getType());
        Stream.of(statusesGroupByMachineType).forEach(stringArrayListEntry -> {
          MachineStatusFragment machineStatusFragment = new MachineStatusFragment();
          machineStatusFragment.setMachineStatuseDataSets(stringArrayListEntry.getValue());
          mMachineSectionsPagerAdapter.addFragment(machineStatusFragment, stringArrayListEntry.getKey());
        });
        machineStatusList = Stream.of(statusesGroupByMachineType.values()).flatMap(machineStatuses -> Stream.of(machineStatuses)).collect(Collectors.toList());
        mViewPager.setAdapter(mMachineSectionsPagerAdapter);
        mMachineSectionsPagerAdapter.notifyDataSetChanged();
        snackbarInfo.dismiss();
      }

      @Override
      public void onFailure(Call<JsonElement> call, Throwable t) {
        Log.d(getLocalClassName(), t.getMessage());
      }
    };
  }

  @Override
  public void onPermissionGranted() {
    Log.d(getLocalClassName(), "Permission granted!");
  }

  @Override
  public void onPermissionDenied(ArrayList<String> deniedPermissions) {
    Snackbar.make(mViewPager, R.string.if_no_permission, Snackbar.LENGTH_LONG).show();
  }

  @Override
  public void onSent(MachineStatus machineStatus) {
    // set machine scan status to true on client
    Stream.of(statusesGroupByMachineType.get(machineStatus.getMachineGroup().getName()))
        .filter(value -> machineStatus.getId() == value.getId())
        .findFirst()
        .get()
        .setReportStatus(true);

    Stream.of(mMachineSectionsPagerAdapter.getFragments()).forEach(fragment -> {
      MachineStatusFragment machineStatusFragment = (MachineStatusFragment) fragment;
      if (machineStatusFragment.getmAdapter() != null)
        machineStatusFragment.getmAdapter().notifyDataSetChanged();
    });
  }
}
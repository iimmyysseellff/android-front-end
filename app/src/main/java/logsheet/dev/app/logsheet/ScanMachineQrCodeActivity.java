package logsheet.dev.app.logsheet;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Vibrator;
import android.widget.Switch;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import butterknife.BindView;
import butterknife.ButterKnife;
import logsheet.dev.app.logsheet.views.PointsOverlayView;

public class ScanMachineQrCodeActivity extends Activity implements QRCodeReaderView.OnQRCodeReadListener {

  @BindView(R.id.qrdecoderview)
  QRCodeReaderView qrCodeReaderView;
  @BindView(R.id.flashlight_switch)
  Switch flashlightCheckBox;
  @BindView(R.id.points_overlay_view)
  PointsOverlayView pointsOverlayView;

  private Intent intent;
  private Vibrator vibrator;

  public static final int ACTIVITY_RESULT_CODE = 0x01011;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_scan_machine_qrcode);
    ButterKnife.bind(this);

    this.intent = new Intent();
    this.vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

    qrCodeReaderView.setAutofocusInterval(3000L);
    qrCodeReaderView.setOnQRCodeReadListener(this);
    qrCodeReaderView.setBackCamera();
    qrCodeReaderView.setQRDecodingEnabled(true);
    flashlightCheckBox.setOnCheckedChangeListener((compoundButton, isChecked) ->
        qrCodeReaderView.setTorchEnabled(isChecked));
    qrCodeReaderView.startCamera();

  }

  @Override
  protected void onResume() {
    super.onResume();
    qrCodeReaderView.startCamera();
  }

  @Override
  protected void onPause() {
    super.onPause();
    qrCodeReaderView.stopCamera();
  }

  @Override
  public void onQRCodeRead(String text, PointF[] points) {
    qrCodeReaderView.stopCamera();
    pointsOverlayView.setPoints(points);
    if (vibrator != null)
      vibrator.vibrate(100L);
    intent.putExtra(Constants.QR_CODE_RESULT_INTENT_KEY, text);
    setResult(Activity.RESULT_OK, intent);
    finish();
  }
}

package logsheet.dev.app.logsheet.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by khairulimam on 15/03/18.
 */

public class MachineSectionPagerAdapter extends FragmentPagerAdapter {

  private List<String> titles = new ArrayList<>();
  private List<Fragment> fragments = new ArrayList<>();

  public MachineSectionPagerAdapter(FragmentManager fm) {
    super(fm);
  }

  public void addFragment(Fragment newFragment, String title) {
    fragments.add(newFragment);
    titles.add(title);
  }

  public List<Fragment> getFragments() {
    return fragments;
  }

  @Override
  public Fragment getItem(int position) {
    return fragments.get(position);
  }

  @Override
  public CharSequence getPageTitle(int position) {
    return titles.get(position);
  }

  @Override
  public int getCount() {
    return fragments.size();
  }
}

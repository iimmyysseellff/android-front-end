package logsheet.dev.app.logsheet.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import logsheet.dev.app.logsheet.R;
import logsheet.dev.app.logsheet.adapters.holders.CardViewMachineStatusHolder;
import logsheet.dev.app.logsheet.api.model.MachineStatus;

/**
 * Created by khairulimam on 15/03/18.
 */

public class RecyclerViewMachineStatsAdapter extends RecyclerView.Adapter<CardViewMachineStatusHolder> {

  private ArrayList<MachineStatus> machineStatuses;

  private ArrayList<MachineStatus> getMachineStatuses() {
    return machineStatuses;
  }

  public RecyclerViewMachineStatsAdapter(ArrayList<MachineStatus> machineStatuses) {
    this.machineStatuses = machineStatuses;
  }

  @Override
  public CardViewMachineStatusHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_place_holder, parent, false);
    return new CardViewMachineStatusHolder(view);
  }

  @Override
  public void onBindViewHolder(CardViewMachineStatusHolder holder, int position) {
    holder.populateView(getMachineStatuses().get(position));
  }

  @Override
  public int getItemCount() {
    return getMachineStatuses().size();
  }
}

package logsheet.dev.app.logsheet.adapters.holders;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import logsheet.dev.app.logsheet.R;
import logsheet.dev.app.logsheet.api.model.MachineStatus;

/**
 * Created by khairulimam on 15/03/18.
 */

public class CardViewMachineStatusHolder extends RecyclerView.ViewHolder {
  @BindView(R.id.ivMachineStatus)
  ImageView ivMachineStatus;
  @BindView(R.id.tvNamaMesin)
  TextView tvNamaMesin;
  @BindView(R.id.tvPlaceHolder)
  TextView tvPlaceHolder;
  @BindView(R.id.tvInitial)
  TextView tvInitial;
  @BindView(R.id.ivMachineStatusContainer)
  ImageView ivMachineStatusContainer;

  private View view;

  public ImageView getIvMachineStatus() {
    return ivMachineStatus;
  }

  public TextView getTvNamaMesin() {
    return tvNamaMesin;
  }

  public void populateView(MachineStatus model) {
    tvNamaMesin.setText(model.getName());
    String[] namesplitted = model.getName().split(" ");
    String initial = namesplitted[0];
    if (namesplitted.length >= 2)
      initial = String.format("%s%s", initial.charAt(0), namesplitted[1].charAt(0));

    tvPlaceHolder.setText(model.getPlaceholder());
    tvInitial.setText(initial);
    toggleMachineStatus(model.getReportStatus());
  }

  private void toggleMachineStatus(boolean scanned) {
    if (scanned) {
      ivMachineStatusContainer.setImageResource(R.drawable.circle_green);
      ivMachineStatus.setImageResource(R.drawable.ic_done_all_white_24dp);
    } else {
      ivMachineStatusContainer.setImageResource(R.drawable.circle_red);
      ivMachineStatus.setImageResource(R.drawable.ic_clear_white_24dp);
    }
  }

  public CardViewMachineStatusHolder(View view) {
    super(view);
    ButterKnife.bind(this, view);
    this.view = view;
  }

}

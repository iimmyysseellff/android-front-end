package logsheet.dev.app.logsheet.api;

import logsheet.dev.app.logsheet.api.call.API;

/**
 * Created by khairulimam on 14/03/18.
 */

public class APIConsumer {
  private static API api;

  public static API consume() {
    if (api == null)
      api = CreateRetrofit.instance().create(API.class);
    return api;
  }
}

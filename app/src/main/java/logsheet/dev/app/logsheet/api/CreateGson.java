package logsheet.dev.app.logsheet.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by khairulimam on 15/03/18.
 */

public class CreateGson {
  private static Gson gson;

  public static Gson instance() {
    if (gson == null)
      gson = new GsonBuilder().setPrettyPrinting().create();
    return gson;
  }
}

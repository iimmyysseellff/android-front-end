package logsheet.dev.app.logsheet.api;

import logsheet.dev.app.logsheet.Constants;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by khairulimam on 14/03/18.
 */

public class CreateRetrofit {
  private static Retrofit retrofit;

  public static Retrofit instance() {
    if (retrofit == null)
      retrofit = new Retrofit.Builder()
          .baseUrl(Constants.BASE_URL)
          .addConverterFactory(GsonConverterFactory
              .create(CreateGson.instance()))
          .build();
    return retrofit;
  }
}

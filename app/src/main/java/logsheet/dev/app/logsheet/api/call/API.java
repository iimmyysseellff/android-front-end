package logsheet.dev.app.logsheet.api.call;

import com.google.gson.JsonElement;

import java.util.List;

import logsheet.dev.app.logsheet.api.model.Machine;
import logsheet.dev.app.logsheet.api.model.MachineGroup;
import logsheet.dev.app.logsheet.api.model.MachineLog;
import logsheet.dev.app.logsheet.api.model.StoreLogMachine;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by khairulimam on 14/03/18.
 */

public interface API {
  @GET("machine-group")
  Call<List<MachineGroup>> listMachineGroups();

  @GET("machine/status/today/bycategory")
  Call<JsonElement> machineLogs();

  @GET("machine")
  Call<List<Machine>> machines();

  @POST("machinelog/log")
  Call<MachineLog> logMachine(@Body StoreLogMachine storeLogMachine);
}

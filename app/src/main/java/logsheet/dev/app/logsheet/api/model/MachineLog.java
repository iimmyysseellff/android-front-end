package logsheet.dev.app.logsheet.api.model;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MachineLog {

  @SerializedName("operator")
  @Expose
  private String operator;
  @SerializedName("machine_id")
  @Expose
  private Integer machineId;
  @SerializedName("log_data")
  @Expose
  private JsonElement logData;
  @SerializedName("updated_at")
  @Expose
  private String updatedAt;
  @SerializedName("created_at")
  @Expose
  private String createdAt;
  @SerializedName("id")
  @Expose
  private Integer id;

  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public Integer getMachineId() {
    return machineId;
  }

  public void setMachineId(Integer machineId) {
    this.machineId = machineId;
  }

  public JsonElement getLogData() {
    return logData;
  }

  public void setLogData(JsonElement logData) {
    this.logData = logData;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

}
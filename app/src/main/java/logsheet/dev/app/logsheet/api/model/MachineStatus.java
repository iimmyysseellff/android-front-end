package logsheet.dev.app.logsheet.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

public class MachineStatus implements Parcelable {

  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("placeholder")
  @Expose
  private String placeholder;
  @SerializedName("classname")
  @Expose
  private String classname;
  @SerializedName("machine_group_id")
  @Expose
  private Integer machineGroupId;
  @SerializedName("created_at")
  @Expose
  private String createdAt;
  @SerializedName("updated_at")
  @Expose
  private String updatedAt;
  @SerializedName("report_status")
  @Expose
  private Boolean reportStatus;
  @SerializedName("machine_group")
  @Expose
  private MachineGroup machineGroup;

  protected MachineStatus(Parcel in) {
    if (in.readByte() == 0) {
      id = null;
    } else {
      id = in.readInt();
    }
    name = in.readString();
    placeholder = in.readString();
    classname = in.readString();
    if (in.readByte() == 0) {
      machineGroupId = null;
    } else {
      machineGroupId = in.readInt();
    }
    createdAt = in.readString();
    updatedAt = in.readString();
    byte tmpReportStatus = in.readByte();
    reportStatus = tmpReportStatus == 0 ? null : tmpReportStatus == 1;
  }

  public static final Creator<MachineStatus> CREATOR = new Creator<MachineStatus>() {
    @Override
    public MachineStatus createFromParcel(Parcel in) {
      return new MachineStatus(in);
    }

    @Override
    public MachineStatus[] newArray(int size) {
      return new MachineStatus[size];
    }
  };

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPlaceholder() {
    if (placeholder == null) return "";
    return placeholder;
  }

  public void setPlaceholder(String placeholder) {
    this.placeholder = placeholder;
  }

  public String getClassname() {
    return classname;
  }

  public void setClassname(String classname) {
    this.classname = classname;
  }

  public Integer getMachineGroupId() {
    return machineGroupId;
  }

  public void setMachineGroupId(Integer machineGroupId) {
    this.machineGroupId = machineGroupId;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getReportStatus() {
    return reportStatus;
  }

  public void setReportStatus(Boolean reportStatus) {
    this.reportStatus = reportStatus;
  }

  public MachineGroup getMachineGroup() {
    return machineGroup;
  }

  public void setMachineGroup(MachineGroup machineGroup) {
    this.machineGroup = machineGroup;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    if (id == null) {
      dest.writeByte((byte) 0);
    } else {
      dest.writeByte((byte) 1);
      dest.writeInt(id);
    }
    dest.writeString(name);
    dest.writeString(placeholder);
    dest.writeString(classname);
    if (machineGroupId == null) {
      dest.writeByte((byte) 0);
    } else {
      dest.writeByte((byte) 1);
      dest.writeInt(machineGroupId);
    }
    dest.writeString(createdAt);
    dest.writeString(updatedAt);
    dest.writeByte((byte) (reportStatus == null ? 0 : reportStatus ? 1 : 2));
  }

  public String getTitle() {
    return String.format("%s %s", name, placeholder);
  }

  @Override
  public String toString() {
    return String.format("[%s, %s, %s]", getName(), getMachineGroup(), getReportStatus());
  }
}
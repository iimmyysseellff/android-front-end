package logsheet.dev.app.logsheet.api.model;

/**
 * Created by khairulimam on 16/03/18.
 */

public class StoreLogMachine {
  private int machine_id;
  private String operator;
  private String log_data;

  public StoreLogMachine() {
  }

  public StoreLogMachine(int machine_id, String operator, String log_data) {

    this.machine_id = machine_id;
    this.operator = operator;
    this.log_data = log_data;
  }

  public int getMachine_id() {

    return machine_id;
  }

  public void setMachine_id(int machine_id) {
    this.machine_id = machine_id;
  }

  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public String getLog_data() {
    return log_data;
  }

  public void setLog_data(String log_data) {
    this.log_data = log_data;
  }
}

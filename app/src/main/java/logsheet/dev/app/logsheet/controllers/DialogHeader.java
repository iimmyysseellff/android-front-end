package logsheet.dev.app.logsheet.controllers;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import logsheet.dev.app.logsheet.R;

/**
 * Created by khairulimam on 16/03/18.
 */

public class DialogHeader {

  @BindView(R.id.tbDialog)
  Toolbar toolbar;
  @BindView(R.id.tvInputRequiredAlert)
  TextView tvAlertDialogRequired;

  private static DialogHeader dialogHeader;
  private static final long WAIT_INTERVAL = 3000L;

  public static void construct(View view, String title, String subtitle) {
    if (dialogHeader == null)
      dialogHeader = new DialogHeader();
    ButterKnife.bind(dialogHeader, view);
    dialogHeader.toolbar.setTitle(title);
    dialogHeader.toolbar.setSubtitle(subtitle);
  }

  public static void showInputRequiredAlert() {
    if (dialogHeader != null) {
      dialogHeader.tvAlertDialogRequired.setVisibility(View.VISIBLE);
    }
  }
}

package logsheet.dev.app.logsheet.controllers;

import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import logsheet.dev.app.logsheet.R;
import logsheet.dev.app.logsheet.api.model.MachineStatus;
import logsheet.dev.app.logsheet.controllers.forms.FormLogic;
import logsheet.dev.app.logsheet.interfaces.AfterDataSent;

/**
 * Created by khairulimam on 17/03/18.
 */

public class FormModalDialogController {

  private static boolean canDismiss = true;
  private Context context;
  private AlertDialog.Builder alertDialog;
  private FormLogic formController;
  private String title, subtitle;
  private View parentView;
  private MachineStatus machineStatus;
  private AfterDataSent afterDataSent;
  private DialogInterface.OnClickListener positiveButton;

  public FormModalDialogController(Context context, View parentView, AfterDataSent afterDataSent) {
    this.context = context;
    this.parentView = parentView;
    this.afterDataSent = afterDataSent;
    alertDialog = new AlertDialog.Builder(context);
    alertDialog.setCancelable(false);
    alertDialog.setNegativeButton(R.string.back_text, (dialog, which) -> dialog.dismiss());
  }

  public void setUpFormDialogForMachine(MachineStatus machineStatus) {
    this.machineStatus = machineStatus;
    Log.d(getClass().getName()+" clss for", machineStatus.getClassname());
    constructFormControllerforClassName(machineStatus.getClassname());
    showDialog();
  }

  private void constructFormControllerforClassName(String className) {
    try {
      Class classFormLogic = Class.forName(className);
      formController = (FormLogic) classFormLogic.newInstance();
      formController.constructAndBootView(context, machineStatus, parentView);
      formController.setAfterDataSent(this.afterDataSent);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (InstantiationException e) {
      e.printStackTrace();
    }
    setUpDialogWithConstructedController();
  }

  private void setUpDialogWithConstructedController() {
    alertDialog.setView(formController.getFormView());
    alertDialog.setPositiveButton(R.string.save_text, getPositiveButton());
  }

  private void showDialog() {
    alertDialog.show();
  }

  private DialogInterface.OnClickListener getPositiveButton() {
    return (dialog, which) -> {
      if (formController.isAnyEmptyForm()) {
        Snackbar snackbar = Snackbar.make(parentView, R.string.error_input_required, 5000);
        TextView textView = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(R.color.red_light));
        snackbar.show();
        return;
      }
      Snackbar.make(parentView, R.string.sending_data_text, Snackbar.LENGTH_LONG).show();
      formController.sendData();
      dialog.dismiss();
    };
  }
}

package logsheet.dev.app.logsheet.controllers.forms;

import android.widget.EditText;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import logsheet.dev.app.logsheet.R;

/**
 * Created by khairulimam on 16/03/18.
 */

public class BFP extends FormLogic {

  private HashMap<String, String> logData = new HashMap<>();

  @BindView(R.id.etSuch)
  EditText etSuch;
  @BindView(R.id.etDisc)
  EditText etDisc;
  @BindView(R.id.etBalance)
  EditText etBalance;

  @Override
  protected void initView() {
    ButterKnife.bind(this, getFormView());
  }

  @Override
  protected HashMap<String, String> getInputs() {
    logData.put("such", etSuch.getText().toString());
    logData.put("disc", etDisc.getText().toString());
    logData.put("balance", etBalance.getText().toString());
    return logData;
  }

  @Override
  protected int getLayout() {
    return R.layout.form_bfp;
  }
}

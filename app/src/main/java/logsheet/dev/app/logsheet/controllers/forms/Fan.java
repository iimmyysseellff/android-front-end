package logsheet.dev.app.logsheet.controllers.forms;

import android.widget.EditText;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import logsheet.dev.app.logsheet.R;

/**
 * Created by khairulimam on 16/03/18.
 */

public class Fan extends FormLogic {

  private HashMap<String, String> logData = new HashMap<>();

  @BindView(R.id.etOil)
  EditText etOil;
  @BindView(R.id.etTmpA)
  EditText etTerperatureA;
  @BindView(R.id.etTmpB)
  EditText etTerperatureB;

  @Override
  protected void initView() {
    ButterKnife.bind(this, getFormView());
  }

  @Override
  protected HashMap<String, String> getInputs() {
    logData.put("oil", etOil.getText().toString());
    logData.put("temperature bearing a", etTerperatureA.getText().toString());
    logData.put("temperature bearing b", etTerperatureB.getText().toString());
    return logData;
  }

  @Override
  protected int getLayout() {
    return R.layout.form_fan;
  }
}

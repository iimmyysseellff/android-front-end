package logsheet.dev.app.logsheet.controllers.forms;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.HashMap;

import logsheet.dev.app.logsheet.Constants;
import logsheet.dev.app.logsheet.R;
import logsheet.dev.app.logsheet.api.APIConsumer;
import logsheet.dev.app.logsheet.api.CreateGson;
import logsheet.dev.app.logsheet.api.model.MachineLog;
import logsheet.dev.app.logsheet.api.model.MachineStatus;
import logsheet.dev.app.logsheet.api.model.StoreLogMachine;
import logsheet.dev.app.logsheet.controllers.DialogHeader;
import logsheet.dev.app.logsheet.interfaces.AfterDataSent;
import logsheet.dev.app.logsheet.interfaces.LogData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by khairulimam on 17/03/18.
 */

public class FormLogic implements LogData {

  protected View formView;
  private View parentView;
  private MachineStatus machineStatus;
  private LayoutInflater layoutInflater;
  private AfterDataSent afterDataSent;
  private Context context;
  private SharedPreferences sharedPreferences;


  public void constructAndBootView(Context context, MachineStatus machineStatus, View parentView) {
    this.context = context;
    sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF_NAME, context.MODE_PRIVATE);
    this.machineStatus = machineStatus;
    this.parentView = parentView;
    layoutInflater = LayoutInflater.from(context);
    this.afterDataSent = afterDataSent;
    bootView();
  }

  public void setAfterDataSent(AfterDataSent afterDataSent) {
    this.afterDataSent = afterDataSent;
  }

  public void setParentView(View parentView) {
    this.parentView = parentView;
  }

  public String getTitle() {
    return machineStatus.getName();
  }

  public String getSubtitle() {
    return machineStatus.getPlaceholder();
  }

  public int getMachineId() {
    return machineStatus.getId();
  }

  public String getDataSetKey() {
    return machineStatus.getMachineGroup().getName();
  }

  protected int getLayout() {
    throw new RuntimeException("Method ini harus diimplementasi");
  }

  public boolean isAnyEmptyForm() {
    return com.annimon.stream.Stream.of(getInputs().values())
        .anyMatch(value -> (value.length() <= 0) || (value == null));
  }

  // 1. call this method once object constructed
  public void bootView() {
    formView = layoutInflater.inflate(getLayout(), null, false);
    initView();
    DialogHeader.construct(getFormView(), getTitle(), getSubtitle());
  }

  protected HashMap<String, String> getInputs() {
    throw new RuntimeException("Method ini harus diimplementasi");
  }

  protected void initView() {
    throw new RuntimeException("Method ini harus diimplementasi");
  }

  public View getFormView() {
    return this.formView;
  }

  @Override
  public String getLogData() {
    return CreateGson.instance().toJson(getInputs());
  }

  String getOperatorName() {
    return sharedPreferences.getString(Constants.OPERATOR_NAME_KEY, Constants.defaultOperatorName);
  }

  protected StoreLogMachine getStoreLogMachine() {
    return new StoreLogMachine(getMachineId(), getOperatorName(), getLogData());
  }

  public void sendData() {
    APIConsumer.consume().logMachine(getStoreLogMachine()).enqueue(new Callback<MachineLog>() {
      @Override
      public void onResponse(Call<MachineLog> call, Response<MachineLog> response) {
        Snackbar s = Snackbar.make(parentView, "", Snackbar.LENGTH_LONG);
        TextView v = s.getView().findViewById(android.support.design.R.id.snackbar_text);
        if (response.code() == 412) {
          s.setText(context.getResources().getString(R.string.machine_did_scanned, machineStatus.getName(), machineStatus.getPlaceholder()));
          v.setTextColor(context.getResources().getColor(R.color.red_light));
        } else {
          s.setText(context.getResources().getString(R.string.machine_data_sent_text, getTitle(), getSubtitle()));
          v.setTextColor(context.getResources().getColor(R.color.green));
        }
        s.show();
        if (afterDataSent != null)
          afterDataSent.onSent(machineStatus);
      }

      @Override
      public void onFailure(Call<MachineLog> call, Throwable t) {
        Snackbar.make(parentView, t.getMessage(), Snackbar.LENGTH_LONG).show();
      }
    });
  }

}

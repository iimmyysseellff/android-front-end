package logsheet.dev.app.logsheet.controllers.forms;

import android.widget.EditText;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import logsheet.dev.app.logsheet.R;

/**
 * Created by khairulimam on 16/03/18.
 */

public class LevelMot extends FormLogic {

  private HashMap<String, String> logData = new HashMap<>();

  @BindView(R.id.etMM)
  EditText etMM;

  @Override
  protected void initView() {
    ButterKnife.bind(this, getFormView());
  }

  @Override
  protected HashMap<String, String> getInputs() {
    logData.put("mm", etMM.getText().toString());
    return logData;
  }

  @Override
  protected int getLayout() {
    return R.layout.form_level_mot;
  }
}

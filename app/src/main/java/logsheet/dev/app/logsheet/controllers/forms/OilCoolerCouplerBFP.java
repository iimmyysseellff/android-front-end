package logsheet.dev.app.logsheet.controllers.forms;

import android.widget.EditText;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import logsheet.dev.app.logsheet.R;

/**
 * Created by khairulimam on 16/03/18.
 */

public class OilCoolerCouplerBFP extends FormLogic {

  private HashMap<String, String> logData = new HashMap<>();

  @BindView(R.id.etInPress)
  EditText etInPress;
  @BindView(R.id.etInTemperature)
  EditText etinTemperature;
  @BindView(R.id.etOutPress)
  EditText etOutPress;
  @BindView(R.id.etOutTemperature)
  EditText etOutTemperature;
  @BindView(R.id.etLevel)
  EditText etLevel;

  @Override
  protected void initView() {
    ButterKnife.bind(this, getFormView());
  }

  @Override
  protected HashMap<String, String> getInputs() {
    logData.put("press in", etInPress.getText().toString());
    logData.put("temperature in", etinTemperature.getText().toString());
    logData.put("press out", etOutPress.getText().toString());
    logData.put("temperature out", etOutTemperature.getText().toString());
    logData.put("level", etLevel.getText().toString());
    return logData;
  }

  @Override
  protected int getLayout() {
    return R.layout.form_oil_cooler_coupler;
  }
}

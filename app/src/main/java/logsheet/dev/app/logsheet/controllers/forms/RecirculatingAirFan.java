package logsheet.dev.app.logsheet.controllers.forms;

import android.widget.EditText;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import logsheet.dev.app.logsheet.R;

/**
 * Created by khairulimam on 16/03/18.
 */

public class RecirculatingAirFan extends FormLogic {

  private HashMap<String, String> logData = new HashMap<>();

  @BindView(R.id.etOilA)
  EditText etOilA;
  @BindView(R.id.etOilB)
  EditText etOilB;
  @BindView(R.id.etTmp)
  EditText etTemperature;

  @Override
  protected void initView() {
    ButterKnife.bind(this, getFormView());
  }

  @Override
  protected HashMap<String, String> getInputs() {
    logData.put("oil level a", etOilA.getText().toString());
    logData.put("oil level b", etOilB.getText().toString());
    logData.put("temperature bearing", etTemperature.getText().toString());
    return logData;
  }

  @Override
  protected int getLayout() {
    return R.layout.form_recirculating_air_fan;
  }
}

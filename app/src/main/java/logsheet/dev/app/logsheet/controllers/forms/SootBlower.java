package logsheet.dev.app.logsheet.controllers.forms;

import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import logsheet.dev.app.logsheet.R;

/**
 * Created by khairulimam on 16/03/18.
 */

public class SootBlower extends FormLogic {

  private HashMap<String, String> logData = new HashMap<>();

  @BindView(R.id.radioGroup)
  RadioGroup radioGroup;

  @Override
  protected void initView() {
    ButterKnife.bind(this, getFormView());
  }

  @Override
  protected HashMap<String, String> getInputs() {
    RadioButton radioButton = getFormView().findViewById(radioGroup.getCheckedRadioButtonId());
    logData.put("group", radioButton.getText().toString());
    return logData;
  }

  @Override
  protected int getLayout() {
    return R.layout.form_soot_blower;
  }
}

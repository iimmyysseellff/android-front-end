package logsheet.dev.app.logsheet.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import logsheet.dev.app.logsheet.R;
import logsheet.dev.app.logsheet.adapters.RecyclerViewMachineStatsAdapter;
import logsheet.dev.app.logsheet.api.model.MachineStatus;

/**
 * Created by khairulimam on 15/03/18.
 */

public class MachineStatusFragment extends Fragment {

  @BindView(R.id.rvMachines)
  RecyclerView mRecyclerView;

  private RecyclerView.LayoutManager mLayoutManager;
  private RecyclerViewMachineStatsAdapter mAdapter;

  private ArrayList<MachineStatus> machineStatuseDataSets;

  public MachineStatusFragment() {
  }

  public void setMachineStatuseDataSets(ArrayList<MachineStatus> machineStatuseDataSets) {
    this.machineStatuseDataSets = machineStatuseDataSets;
  }

  public RecyclerViewMachineStatsAdapter getmAdapter() {
    return mAdapter;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater,
                           @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_machine_machine_status, container, false);
    ButterKnife.bind(this, rootView);

    mLayoutManager = new LinearLayoutManager(getContext());
    mRecyclerView.setLayoutManager(mLayoutManager);

    mAdapter = new RecyclerViewMachineStatsAdapter(this.machineStatuseDataSets);
    mRecyclerView.setAdapter(mAdapter);

    return rootView;
  }
}

package logsheet.dev.app.logsheet.interfaces;

import logsheet.dev.app.logsheet.api.model.MachineStatus;

/**
 * Created by khairulimam on 18/03/18.
 */

public interface AfterDataSent {
  void onSent(MachineStatus machineStatus);
}
